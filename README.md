# UNSC-Graph

## An extensible knowledge graph for the UNSC corpus.

This graph contains data for use with the UNSC corpus available through the Harvard Dataverse at <https://dataverse.harvard.edu/dataset.xhtml?persistentId=doi:10.7910/DVN/KGVSYH>

It was created with SWI-Prolog and currently consists of the sets of facts described below. An example file for using Prolog in Python with the [PySwip library](https://github.com/yuce/pyswip) is included in the repository. The file `unscgraph.pl` is a module; it will load all data in the graph, and defines some rules to assist with queries.

### `country_as_tag.pl`

Contains the Wikidata-ID (W-ID) of countries used as tags by the UN library, mapped to each meeting (S/PV is the meeting identifier).

Predicate: `country_as_tag/2`  
Argument 1: S/PV  
Argument 2: W-ID

### `country_as_topic.pl`

Contains the W-ID of countries listed as a topic by the UN Repertoire, mapped to each meeting.

Predicate: `country_as_topic/2`  
Argument 1: S/PV  
Argument 2: W-ID  

### `country_mention.pl`

Contains detected mentions of countries in the speech text.

Predicate: `country_mention/6`  
Argument 1: W-ID of mentioning country  
Argument 2: W-ID of mentioned country  
Argument 3: S/PV  
Argument 4: Speech number  
Argument 5: Paragraph number  
Argument 6: Sentence number

### `membership.pl`

Contains membership information on all members of the Security Council from 1995 to 2023.

Predicate: `membership/6`  
Argument 1: Permanent/non-permanent  
Argument 2: Regional group  
Argument 3: Arab nations' representative (yes/no)  
Argument 4: W-ID of country  
Argument 5: From-date  
Argument 6: To-date

### `presidency.pl`

Contains the W-ID of the country of the meeting president for each meeting.

Predicate: `presidency/2`  
Argument 1: S/PV  
Argument 2: W-ID of country

### `sentences.pl`

Contains individual sentences from all speeches.

Predicate: `sentence/5`  
Argument 1: Sentence string  
Argument 2: S/PV  
Argument 3: Speech number  
Argument 4: Paragraph number  
Argument 5: Sentence number 

### `speech_sentiment.pl`

Contains the average sentiment score for each speech.

Predicate: `speech_sentiment/3`  
Argument 1: S/PV  
Argument 2: Speech number  
Argument 3: Sentiment score

### `spv_date.pl`

Contains dates for all meetings.

Predicate: `spv_date/2`  
Argument 1: S/PV  
Argument 2: Date

### `spv_speech_wid.pl`

Contains the W-ID of the country for each speech.

Predicate: `spv_speech_wid/3`  
Argument 1: S/PV  
Argument 2: Speech number  
Argument 3: W-ID of country

### `spv_tag.pl`

Contains the tags supplied by the UN library as strings, mapped to each meeting.

Predicate: `spv_tag/2`  
Argument 1: S/PV  
Argument 2: Tag

### `top_level_topic.pl`

Contains the top level topic category as defined by the UN Repertoire for each meeting. These are one of: Africa, Americas, Asia, Europe, Middle East, Thematic.

Predicate: `spv_tlt/2`  
Argument 1: S/PV  
Argument 2: Topic

### `unterm.pl`

Contains the official term as used by the UN Repertoire for each country, mapped to that country's W-ID.

Predicate: `wid_unterm/2`  
Argument 1: W-ID of country  
Argument 2: UN-term for the country

### `wdlabel.pl`

Contains the main label as used by Wikidata for each country, mapped to that country's W-ID.

Predicate: `wid_wdlabel/2`  
Argument 1: W-ID of country  
Argument 2: Wikidata-label for the country

### `wid_alias.pl`

Contains all aliases (that are four characters or longer) for each country, as provided by Wikidata, mapped to that country's W-ID.

Predicate: `wid_alias/2`  
Argument 1: W-ID of country  
Argument 2: Alias for the country


### `wid_neighbour.pl`

Contains all neighbouring countries for each country, as provided by Wikidata, mapped to their respective W-ID. The predicate below is symmetrical.

Predicate: `wid_neighbour/2`  
Argument 1: W-ID of one of the countries  
Argument 2: W-ID of the other country
