wid_alias('Q145','G B R').
wid_alias('Q145','G.B.R.').
wid_alias('Q145','Great Britain and Northern Ireland').
wid_alias('Q145','Britain').
wid_alias('Q145','U. K.').
wid_alias('Q145','United Kingdom Of Great Britain And Northern Ireland').
wid_alias('Q145','United Kingdom of Great Britain and Northern Ireland').
wid_alias('Q145','United Kingdom').
wid_alias('Q145','The United Kingdom of Great Britain and Northern Ireland').
wid_alias('Q145','G. B.').
wid_alias('Q145','U.K.').
wid_alias('Q145','Great Britain').
wid_alias('Q145','G.B.').
wid_alias('Q145','The UK').
wid_alias('Q30','the United States').
wid_alias('Q30','the U.S.').
wid_alias('Q30','the United States of America').
wid_alias('Q30','U.S.A.').
wid_alias('Q30','United States').
wid_alias('Q30','the US of America').
wid_alias('Q30','United States Of America').
wid_alias('Q30','the States').
wid_alias('Q30','the U.S. of A').
wid_alias('Q30','America').
wid_alias('Q30','U. S. A.').
wid_alias('Q30','US of A').
wid_alias('Q30','the USA').
wid_alias('Q30','U.S. of America').
wid_alias('Q30','the US of A').
wid_alias('Q30','U.S.').
wid_alias('Q30','the US').
wid_alias('Q30','the U.S. of America').
wid_alias('Q30','United States of America').
wid_alias('Q30','U. S.').
wid_alias('Q30','US of America').
wid_alias('Q30','the U.S.A.').
wid_alias('Q159','Federation of Russia').
wid_alias('Q159','Rossiya').
wid_alias('Q159','Rossiyskaya Federatsiya').
wid_alias('Q159','Russia').
wid_alias('Q159','Russian Federation').
wid_alias('Q159','Rossija').
wid_alias('Q159','Россия').
wid_alias('Q159','Российская Федерация').
wid_alias('Q159','Rossijskaja Federatsija').
wid_alias('Q142','the Hexagon').
wid_alias('Q142','France').
wid_alias('Q142','République française').
wid_alias('Q142','Republic of France').
wid_alias('Q142','La France').
wid_alias('Q142','French Republic').
wid_alias('Q148','China').
wid_alias('Q148','Mainland China').
wid_alias('Q148','China PR').
wid_alias('Q148','People\'s Republic of China').
wid_alias('Q148','PR China').
wid_alias('Q183','Deutschland').
wid_alias('Q183','Bundesrepublik Deutschland').
wid_alias('Q183','BR Deutschland').
wid_alias('Q183','Germany').
wid_alias('Q183','Federal Republic of Germany').
wid_alias('Q17','State of Japan').
wid_alias('Q17','Nihon').
wid_alias('Q17','Nihon-koku').
wid_alias('Q17','Nippon-koku').
wid_alias('Q17','Land of the Rising Sun').
wid_alias('Q17','Nippon').
wid_alias('Q17','Japan').
wid_alias('Q258','Republic of South Africa').
wid_alias('Q258','South Africa').
wid_alias('Q414','Argentina').
wid_alias('Q414','Argentine Republic').
wid_alias('Q414','Republic of Argentina').
wid_alias('Q414','Arjentina').
wid_alias('Q252','Republic of Indonesia').
wid_alias('Q252','Indonesia').
wid_alias('Q419','República del Perú').
wid_alias('Q419','Republic of Peru').
wid_alias('Q419','Peru').
wid_alias('Q419','Republica del Peru').
wid_alias('Q29','Spain').
wid_alias('Q29','Kingdom of Spain').
wid_alias('Q1033','Nigeria').
wid_alias('Q1033','Federal Republic of Nigeria').
wid_alias('Q1033','Naija').
wid_alias('Q843','Republic of Pakistan').
wid_alias('Q843','Islamic Republic of Pakistan').
wid_alias('Q843','Pakistan').
wid_alias('Q31','Koninkrijk België').
wid_alias('Q31','Belgium').
wid_alias('Q31','Royaume de Belgique').
wid_alias('Q31','Belgien').
wid_alias('Q31','Königreich Belgien').
wid_alias('Q31','Kingdom of Belgium').
wid_alias('Q36','Poland').
wid_alias('Q36','Republic of Poland').
wid_alias('Q36','Polska').
wid_alias('Q298','República de Chile').
wid_alias('Q298','Republica de Chile').
wid_alias('Q298','Republic of Chile').
wid_alias('Q298','Chile').
wid_alias('Q79','Rep. Egypt').
wid_alias('Q79','Arab Rep. Egypt').
wid_alias('Q79','Egypt').
wid_alias('Q79','Arab Republic of Egypt').
wid_alias('Q79','Republic of Egypt').
wid_alias('Q34','Sverige').
wid_alias('Q34','Kingdom of Sweden').
wid_alias('Q34','Sweden').
wid_alias('Q34','Konungariket Sverige').
wid_alias('Q833','Federation of Malaysia').
wid_alias('Q833','Malaysia Federation').
wid_alias('Q833','Malaysia').
wid_alias('Q38','Republic of Italy').
wid_alias('Q38','Italian Republic').
wid_alias('Q38','Italia').
wid_alias('Q38','Italy').
wid_alias('Q38','Repubblica Italiana').
wid_alias('Q212','Ukraine').
wid_alias('Q212','Ukrainia').
wid_alias('Q212','Україна').
wid_alias('Q212','the Ukraine').
wid_alias('Q212','Ukr.').
wid_alias('Q212','Ukraina').
wid_alias('Q212','Ukraïna').
wid_alias('Q55','Holland').
wid_alias('Q55','Netherlands').
wid_alias('Q55','the Netherlands').
wid_alias('Q55','Netherlands (after 1945)').
wid_alias('Q55','Nederland').
wid_alias('Q739','ISO 3166-1:CO').
wid_alias('Q739','Colombia').
wid_alias('Q739','Republica de Colombia').
wid_alias('Q739','Colombiá').
wid_alias('Q739','República de Colombia').
wid_alias('Q739','Republic of Colombia').
wid_alias('Q739','The Republic of Colombia').
wid_alias('Q155','Federative Republic of Brazil').
wid_alias('Q155','Brasil').
wid_alias('Q155','Brazil').
wid_alias('Q96','Mexican Republic').
wid_alias('Q96','United Mexican States').
wid_alias('Q96','US of M').
wid_alias('Q96','Mexico').
wid_alias('Q881','Viet Nam').
wid_alias('Q881','Socialist Republic of Vietnam').
wid_alias('Q881','Vietnam').
wid_alias('Q881','Việt Nam').
wid_alias('Q750','Bolivia (Plurinational State Of)').
wid_alias('Q750','Bolivia (Plurinational State of)').
wid_alias('Q750','Republic of Bolivia').
wid_alias('Q750','Bolivia').
wid_alias('Q750','Plurinational State of Bolivia').
wid_alias('Q77','Uruguay').
wid_alias('Q77','República Oriental del Uruguay').
wid_alias('Q77','Oriental Republic of Uruguay').
wid_alias('Q817','Kuwait').
wid_alias('Q817','State of Kuwait').
wid_alias('Q884','Rep. Korea').
wid_alias('Q884','S. Korea').
wid_alias('Q884','South Korea').
wid_alias('Q884','Korea Republic').
wid_alias('Q884','Republic of Korea').
wid_alias('Q884','Hanguk').
wid_alias('Q884','Daehanminguk').
wid_alias('Q884','Daehan Minguk').
wid_alias('Q884','Korea (South)').
wid_alias('Q884','Republic Of Korea').
wid_alias('Q43','Türkiye').
wid_alias('Q43','Republic of Turkey').
wid_alias('Q43','Turkey').
wid_alias('Q43','Republic of Türkiye').
wid_alias('Q408','Aussieland').
wid_alias('Q408','New Hollandia').
wid_alias('Q408','Australia').
wid_alias('Q408','New Holland').
wid_alias('Q408','Nova Hollandia').
wid_alias('Q408','Straya').
wid_alias('Q408','Stralia').
wid_alias('Q408','Commonwealth of Australia').
wid_alias('Q668','India').
wid_alias('Q668','Indostan').
wid_alias('Q668','Bharatvarsh').
wid_alias('Q668','Bharat').
wid_alias('Q668','Bharat Ganarajya').
wid_alias('Q668','Bharata').
wid_alias('Q668','Hindoostan').
wid_alias('Q668','Republic of India').
wid_alias('Q668','Hindustan').
wid_alias('Q16','Dominion of Canada').
wid_alias('Q16','Canada').
wid_alias('Q16','British North America').
wid_alias('Q810','Kingdom of Jordan').
wid_alias('Q810','Yarden').
wid_alias('Q810','Giordania').
wid_alias('Q810','Jordan').
wid_alias('Q810','Hashemite Kingdom of Jordan').
wid_alias('Q45','Portuguese Republic').
wid_alias('Q45','Portugal').
wid_alias('Q858','Syria').
wid_alias('Q858','Syrian Arab Republic').
wid_alias('Q664','Zeelandia Nova').
wid_alias('Q664','New Zealand').
wid_alias('Q664','Nova Zelandia').
wid_alias('Q664','Dominion of New Zealand').
wid_alias('Q664','Aotearoa').
wid_alias('Q664','Aotearoa New Zealand').
wid_alias('Q664','New Zeeland').
wid_alias('Q664','Zelandia Nova').
wid_alias('Q1008','Republic of Cote d\'Ivoire').
wid_alias('Q1008','Ivory Coast').
wid_alias('Q1008','Cote d\'lvoire').
wid_alias('Q1008','Republic of the Ivory Coast').
wid_alias('Q1008','Cote D\'Ivoire').
wid_alias('Q1008','Republic of Côte d\'Ivoire').
wid_alias('Q1008','Cote Ivoire').
wid_alias('Q1008','Cote-d\'Ivoire').
wid_alias('Q1008','Côte d\'Ivoire').
wid_alias('Q1008','Republic of Ivory Coast').
wid_alias('Q1008','Côte d’Ivoire').
wid_alias('Q1008','Cote d\'Ivoire').
wid_alias('Q1008','The Ivory Coast').
wid_alias('Q1008','Côte-d\'Ivoire').
wid_alias('Q1037','Republic of Rwanda').
wid_alias('Q1037','Rwanda').
wid_alias('Q916','Ngola').
wid_alias('Q916','Angola').
wid_alias('Q916','Republic of Angola').
wid_alias('Q232','Kazakhstan').
wid_alias('Q232','Republic of Kazakhstan').
wid_alias('Q232','Qazaqstan').
wid_alias('Q902','People\'s Republic of Bangladesh').
wid_alias('Q902','Bangladesh').
wid_alias('Q786','República Dominicana').
wid_alias('Q786','Dominican Republic').
wid_alias('Q786','Dominicana').
wid_alias('Q786','Republica Dominicana').
wid_alias('Q37','Lietuva').
wid_alias('Q37','Lithuania').
wid_alias('Q37','Republic of Lithuania').
wid_alias('Q37','Lietuvos Respublika').
wid_alias('Q20','Norway').
wid_alias('Q20','Kingdom of Norway').
wid_alias('Q1036','The pearl of Africa').
wid_alias('Q1036','The Republic of Uganda').
wid_alias('Q1036','Republic of Uganda').
wid_alias('Q1036','Uganda').
wid_alias('Q983','Republic of Equatorial Guinea').
wid_alias('Q983','Equatorial Guinea').
wid_alias('Q717','Venezuela').
wid_alias('Q717','BR Venezuela').
wid_alias('Q717','Estados Unidos de Venezuela').
wid_alias('Q717','Venezuela (Bolivarian Republic Of)').
wid_alias('Q717','Bolivarian Republic of Venezuela').
wid_alias('Q717','Vzla').
wid_alias('Q115','Federal Democratic Republic of Ethiopia').
wid_alias('Q115','Ethiopia').
wid_alias('Q1041','Senegal').
wid_alias('Q1041','Republic of Senegal').
wid_alias('Q1028','Kingdom of Morocco').
wid_alias('Q1028','Lmaġrib').
wid_alias('Q1028','Maroc').
wid_alias('Q1028','Morocco').
wid_alias('Q1028','Marocco').
wid_alias('Q1028','al-Maġrib').
wid_alias('Q227','Azərbaycan').
wid_alias('Q227','Azərbaycan Respublikası').
wid_alias('Q227','Republic of Azerbaijan').
wid_alias('Q227','Azerbaijan').
wid_alias('Q822','Lubnan').
wid_alias('Q822','Lebanon').
wid_alias('Q822','Lebanese Republic').
wid_alias('Q822','Republic of Lebanon').
wid_alias('Q948','Tunisia').
wid_alias('Q948','Republic of Tunisia').
wid_alias('Q948','Tunisian Republic').
wid_alias('Q224','Croatia').
wid_alias('Q224','Republic of Croatia').
wid_alias('Q800','Costa Rica').
wid_alias('Q800','Republic of Costa Rica').
wid_alias('Q334','Garden City').
wid_alias('Q334','Lion City').
wid_alias('Q334','S\'pore').
wid_alias('Q334','Republic of Singapore').
wid_alias('Q334','Singapore City').
wid_alias('Q334','Singapore').
wid_alias('Q774','Guatemala').
wid_alias('Q774','Republic of Guatemala').
wid_alias('Q1016','Libya').
wid_alias('Q1016','State of Libya').
wid_alias('Q1030','Republic of Namibia').
wid_alias('Q1030','Namibia').
wid_alias('Q766','Commonwealth of Jamaica').
wid_alias('Q766','Jamaica').
wid_alias('Q801','Israel').
wid_alias('Q801','the Jewish state').
wid_alias('Q801','State of Israel').
wid_alias('Q928','Philippine Islands').
wid_alias('Q928','The Philippines').
wid_alias('Q928','Republic of the Philippines').
wid_alias('Q928','Philippines').
wid_alias('Q1000','Gabonese Republic').
wid_alias('Q1000','Republique Gabonaise').
wid_alias('Q1000','République Gabonaise').
wid_alias('Q1000','Gabon').
wid_alias('Q32','Groussherzogtum Lëtzebuerg').
wid_alias('Q32','Luxemburg').
wid_alias('Q32','Lëtzebuerg').
wid_alias('Q32','Großherzogtum Luxemburg').
wid_alias('Q32','Luxembourg').
wid_alias('Q32','Grand-Duché de Luxembourg').
wid_alias('Q32','Grand Duchy of Luxembourg').
wid_alias('Q846','State of Qatar').
wid_alias('Q846','Qatar').
wid_alias('Q1006','Guinea-Conakry').
wid_alias('Q1006','Republic of Guinea').
wid_alias('Q1006','Guinea').
wid_alias('Q1006','République de Guinée').
wid_alias('Q1006','Republique de Guinee').
wid_alias('Q225','Bosnia and Herzegovina').
wid_alias('Q225','Bosnia And Herzegovina').
wid_alias('Q225','Bosnia & Herzegovina').
wid_alias('Q225','Bosnia').
wid_alias('Q225','Bosnia and Hercegovina').
wid_alias('Q225','Bosnia-Herzegovina').
wid_alias('Q40','Österreich').
wid_alias('Q40','Republik Österreich').
wid_alias('Q40','Republic of Austria').
wid_alias('Q40','Austria').
wid_alias('Q657','Republic of Chad').
wid_alias('Q657','Chad').
wid_alias('Q218','România').
wid_alias('Q218','Romania').
wid_alias('Q218','Rumania').
wid_alias('Q218','Roumania').
wid_alias('Q1009','Cameroon').
wid_alias('Q1009','Republic of Cameroon').
wid_alias('Q35','Danmark').
wid_alias('Q35','metropolitan Denmark').
wid_alias('Q35','Denmark').
wid_alias('Q35','Denmark proper').
wid_alias('Q912','Mali').
wid_alias('Q912','Republic of Mali').
wid_alias('Q945','Togolese Republic').
wid_alias('Q945','Togo').
wid_alias('Q117','Ghana').
wid_alias('Q117','Republic of Ghana').
wid_alias('Q965','Burkina Faso').
wid_alias('Q27','Éire').
wid_alias('Q27','Republic of Ireland').
wid_alias('Q27','Eire').
wid_alias('Q27','Ireland').
wid_alias('Q27','Hibernia').
wid_alias('Q27','Ireland (state)').
wid_alias('Q27','Southern Ireland').
wid_alias('Q27','Ireland, Republic of').
wid_alias('Q794','Iran').
wid_alias('Q794','Islamic Republic of Iran').
wid_alias('Q794','Persia').
wid_alias('Q794','Islamic Republic Of Iran').
wid_alias('Q794','Islamic Rep. Iran').
wid_alias('Q41','Hellenic Republic').
wid_alias('Q41','Greece').
wid_alias('Q41','Hellas').
wid_alias('Q41','Ellada').
wid_alias('Q41','Greek').
wid_alias('Q41','Greek Republic').
wid_alias('Q214','Slovakia (Slovak Republic)').
wid_alias('Q214','Slovakia').
wid_alias('Q214','Slovak Republic').
wid_alias('Q262','People\'s Democratic Republic of Algeria').
wid_alias('Q262','Algeria').
wid_alias('Q1049','Soudan').
wid_alias('Q1049','al-Sudan').
wid_alias('Q1049','North Sudan').
wid_alias('Q1049','Republic of the Sudan').
wid_alias('Q1049','as-Sūdān').
wid_alias('Q1049','the Sudan').
wid_alias('Q1049','as-Sudan').
wid_alias('Q1049','Republic of Sudan').
wid_alias('Q1049','Sudan').
wid_alias('Q215','Slovenia').
wid_alias('Q215','Republika Slovenija').
wid_alias('Q215','Republic of Slovenia').
wid_alias('Q215','Slovenija').
wid_alias('Q963','Botswana').
wid_alias('Q963','Republic of Botswana').
wid_alias('Q963','Lefatshe la Botswana').
wid_alias('Q191','Eesti').
wid_alias('Q191','Estland').
wid_alias('Q191','Estonia').
wid_alias('Q191','Republic of Estonia').
wid_alias('Q191','Eesti Vabariik').
wid_alias('Q219','Bulgaria').
wid_alias('Q219','Country of Cois').
wid_alias('Q219','Republic of Bulgaria').
wid_alias('Q1027','Republic of Mauritius').
wid_alias('Q1027','Maurice').
wid_alias('Q1027','Mauritius').
wid_alias('Q1027','Moris').
wid_alias('Q962','Dahomey').
wid_alias('Q962','Republic of Benin').
wid_alias('Q962','Benin').
wid_alias('Q804','Panama').
wid_alias('Q804','Republic of Panama').
wid_alias('Q219060','Palestinian Authority').
wid_alias('Q219060','State of Palestine').
wid_alias('Q219060','Palestine').
wid_alias('Q219060','Palestinian National Authority').
wid_alias('Q241','Republic of Cuba').
wid_alias('Q241','Cuba').
wid_alias('Q241','Governing Council of Iraq').
wid_alias('Q796','Republic of Iraq').
wid_alias('Q796','Komar-i ‘Êraq').
wid_alias('Q796','Iraq').
wid_alias('Q796','al-‘Irāq').
wid_alias('Q796','Jumhūriyyat al-‘Irāq').
wid_alias('Q114','Republic of Kenya').
wid_alias('Q114','Kenya').
wid_alias('Q39','Swiss').
wid_alias('Q39','Confoederatio Helvetica').
wid_alias('Q39','Switzerland').
wid_alias('Q39','Swiss Confederation').
wid_alias('Q398','Bahrein Islands').
wid_alias('Q398','Kingdom of Bahrain').
wid_alias('Q398','Bahrain').
wid_alias('Q1005','The Gambia').
wid_alias('Q1005','Republic of The Gambia').
wid_alias('Q1005','Gambia').
wid_alias('Q1005','Islamic Republic of the Gambia').
wid_alias('Q347','Principality of Liechtenstein').
wid_alias('Q347','Liechtenstein').
wid_alias('Q1032','Republic of Niger').
wid_alias('Q1032','Republic Niger').
wid_alias('Q1032','Niger').
wid_alias('Q1032','Republic of the Niger').
wid_alias('Q1032','The Niger').
wid_alias('Q783','Honduras').
wid_alias('Q783','Republic of Honduras').
wid_alias('Q783','Honduran Republic').
wid_alias('Q757','Saint Vincent').
wid_alias('Q757','St. Vincent').
wid_alias('Q757','Saint Vincent And The Grenadines').
wid_alias('Q757','St Vincent').
wid_alias('Q757','St Vincent and the Grenadines').
wid_alias('Q757','Saint Vincent and the Grenadines').
wid_alias('Q757','St. Vincent and the Grenadines').
wid_alias('Q889','Afghanistan').
wid_alias('Q403','Serbia').
wid_alias('Q403','Srbija').
wid_alias('Q403','Republic of Serbia').
wid_alias('Q403','Republika Srbija').
wid_alias('Q1007','Guinea-Bissau').
wid_alias('Q1007','Republic of Guinea-Bissau').
wid_alias('Q974','Democratic Republic Of The Congo').
wid_alias('Q974','Zaire').
wid_alias('Q974','Dem. Republic of Congo').
wid_alias('Q974','D.R.C.').
wid_alias('Q974','Congo (Kinshasa)').
wid_alias('Q974','Congo-Kinshasa').
wid_alias('Q974','Democratic Republic of the Congo').
wid_alias('Q974','DR Congo').
wid_alias('Q974','Dem. Republic of the Congo').
wid_alias('Q974','Dem. Rep. Congo').
wid_alias('Q974','Democratic Republic of Congo').
wid_alias('Q851','Saudi Arabia').
wid_alias('Q851','Kingdom of Saudi Arabia').
wid_alias('Q213','Czech Republic').
wid_alias('Q213','Česko').
wid_alias('Q213','Česká republika').
wid_alias('Q213','Czechia').
wid_alias('Q230','Georgia').
wid_alias('Q230','Republic of Georgia').
wid_alias('Q230','Georgia (country)').
wid_alias('Q230','Sakartvelo').
wid_alias('Q230','საქართველო').
wid_alias('Q878','United Arab Emirates').
wid_alias('Q878','the United Arab Emirates').
wid_alias('Q878','the Emirates').
wid_alias('Q878','the U.A.E.').
wid_alias('Q878','Emirates').
wid_alias('Q878','U.A.E.').
wid_alias('Q878','the UAE').
wid_alias('Q869','Kingdom of Thailand').
wid_alias('Q869','Siam').
wid_alias('Q869','Thailand').
wid_alias('Q869','Land of Smiles').
wid_alias('Q1045','Federal Republic of Somalia').
wid_alias('Q1045','Somalia').
wid_alias('Q842','Sultanate of Oman').
wid_alias('Q842','Oman').
wid_alias('Q399','Hayastani Hanrapetut’yun').
wid_alias('Q399','Հայաստանի Հանրապետություն').
wid_alias('Q399','Armenia').
wid_alias('Q399','Hayastan').
wid_alias('Q399','Republic of Armenia').
wid_alias('Q805','Yemen').
wid_alias('Q805','Rep. Yemen').
wid_alias('Q805','Republic of Yemen').
wid_alias('Q1044','Sierra Leone').
wid_alias('Q1044','Republic of Sierra Leone').
wid_alias('Q736','Ecuador').
wid_alias('Q736','Republic of Ecuador').
wid_alias('Q736','Republic of the Equator').
wid_alias('Q736','The Ecuadorian State').
wid_alias('Q854','Sri Lanka').
wid_alias('Q854','Lanka, Sri').
wid_alias('Q854','Ceylan').
wid_alias('Q854','Ceylon').
wid_alias('Q854','Democratic Socialist Republic of Sri Lanka').
wid_alias('Q854','Serendib').
wid_alias('Q854','Taprobane').
wid_alias('Q854','Srilanka').
wid_alias('Q967','Republic of Burundi').
wid_alias('Q967','Gouvernement du Burundi').
wid_alias('Q967','Republika y\'Uburundi').
wid_alias('Q967','Burundi').
wid_alias('Q958','Southern Sudan').
wid_alias('Q958','South Sudan').
wid_alias('Q958','Republic of South Sudan').
wid_alias('Q837','Nepal').
wid_alias('Q837','Federal Democratic Republic of Nepal').
wid_alias('Q159583','Santa Sede').
wid_alias('Q159583','Sancta Sedes').
wid_alias('Q159583','Holy See').
wid_alias('Q159583','Chair of Peter').
wid_alias('Q159583','Holy See of Rome').
wid_alias('Q159583','The Holy See').
wid_alias('Q159583','See of Rome').
wid_alias('Q159583','See of Peter').
wid_alias('Q790','Republic of Haiti').
wid_alias('Q790','République d\'Haïti').
wid_alias('Q790','Haitian Republic').
wid_alias('Q790','Haiti').
wid_alias('Q790','Hayti').
wid_alias('Q790','Ayiti').
wid_alias('Q1246','Republic of Kosovo').
wid_alias('Q1246','Kosovo').
wid_alias('Q712','Fiji').
wid_alias('Q712','Matanitu Tugalala o Viti').
wid_alias('Q712','Fijī Gaṇarājya').
wid_alias('Q712','Fijī').
wid_alias('Q712','Viti').
wid_alias('Q712','Republic of Fiji').
wid_alias('Q712','Republic of the Fiji Islands').
wid_alias('Q929','Republique centrafricaine').
wid_alias('Q929','Central African Republic').
wid_alias('Q929','Centrafrique').
wid_alias('Q929','République centrafricaine').
wid_alias('Q929','Ködörösêse tî Bêafrîka').
wid_alias('Q222','Republika e Shqipërisë').
wid_alias('Q222','Shqipërisë').
wid_alias('Q222','Republic of Albania').
wid_alias('Q222','Albania').
wid_alias('Q222','People\'s Socialist Republic of Albania').
wid_alias('Q222','Republika Popullore Socialiste e Shqiperise').
wid_alias('Q222','Republika Popullore e Shqiperise').
wid_alias('Q222','People\'s Republic of Albania').
wid_alias('Q836','Burma').
wid_alias('Q836','Republic of the Union of Myanmar').
wid_alias('Q836','Myanmar').
wid_alias('Q836','Union of Burma').
wid_alias('Q28','Hungary').
wid_alias('Q189','Republic of Iceland').
wid_alias('Q189','Iceland').
wid_alias('Q189','Island').
wid_alias('Q826','Maldives').
wid_alias('Q826','Republic of Maldives').
wid_alias('Q954','Southern Rhodesia').
wid_alias('Q954','Republic of Zimbabwe').
wid_alias('Q954','Rhodesia').
wid_alias('Q954','Zimbabwe Rhodesia').
wid_alias('Q954','Zimbabwe').
wid_alias('Q1014','Republic of Liberia').
wid_alias('Q1014','Liberia').
wid_alias('Q33','Republic of Finland').
wid_alias('Q33','Šuomi').
wid_alias('Q33','Finnia').
wid_alias('Q33','Finland').
wid_alias('Q33','Suomen tasavalta').
wid_alias('Q33','Suomi').
wid_alias('Q33','Land of Thousand Lakes').
wid_alias('Q33','Republiken Finland').
wid_alias('Q977','Republic of Djibouti').
wid_alias('Q977','Gabuuti').
wid_alias('Q977','Jībūtī').
wid_alias('Q977','Jabuuti').
wid_alias('Q977','Djibouti').
wid_alias('Q792','El Salvador').
wid_alias('Q792','Republic of El Salvador').
wid_alias('Q792','Salvador').
wid_alias('Q811','Republic of Nicaragua').
wid_alias('Q811','Nicaragua').
wid_alias('Q574','Democratic Republic of East Timor').
wid_alias('Q574','Timor-Leste').
wid_alias('Q574','Timor Leste').
wid_alias('Q574','Democratic Republic of Timor-Leste').
wid_alias('Q574','East Timor').
wid_alias('Q184','Belorussia').
wid_alias('Q184','Respublika Belarus\'').
wid_alias('Q184','Рэспубліка Беларусь').
wid_alias('Q184','Bielorussia').
wid_alias('Q184','Byeloruss').
wid_alias('Q184','Bielaruś').
wid_alias('Q184','Республика Беларусь').
wid_alias('Q184','White Ruthenia').
wid_alias('Q184','White Russia').
wid_alias('Q184','Беларусь').
wid_alias('Q184','Byelorussia').
wid_alias('Q184','Belarus').
wid_alias('Q184','Republic of Belarus').
wid_alias('Q184','Respublika Bielaruś').
wid_alias('Q36704','Yugoslavia').
wid_alias('Q36704','Jugoslavija').
wid_alias('Q1029','Republic of Mozambique').
wid_alias('Q1029','Mozambique').
wid_alias('Q1029','Moçambique').
wid_alias('Q1029','República de Moçambique').
wid_alias('Q236','Crna Gora').
wid_alias('Q236','Montenegro').
wid_alias('Q229','Greek Administration of Southern Cyprus').
wid_alias('Q229','Greek Cypriot State').
wid_alias('Q229','Cyprus').
wid_alias('Q229','Republic of Cyprus').
wid_alias('Q953','Republic of Zambia').
wid_alias('Q953','Zambia').
wid_alias('Q211','Latvian Republic').
wid_alias('Q211','Leţmо̄ Vabāmо̄').
wid_alias('Q211','Latveja').
wid_alias('Q211','Latvejas Republika').
wid_alias('Q211','Leţmо̄').
wid_alias('Q211','Republic of Latvia').
wid_alias('Q211','Latvia').
wid_alias('Q211','Latvijas Republika').
wid_alias('Q211','Latvija').
wid_alias('Q863','Tajikistan').
wid_alias('Q863','Republic of Tajikistan').
wid_alias('Q813','Kyrgyzstan').
wid_alias('Q813','Kyrgyz Republic').
wid_alias('Q813','Kirgizia').
wid_alias('Q37024','Serbia-Montenegro').
wid_alias('Q37024','Serbia And Montenegro').
wid_alias('Q37024','Serbia and Montenegro').
wid_alias('Q265','Uzbekistan').
wid_alias('Q265','Republic of Uzbekistan').
wid_alias('Q691','Papua New Guinea').
wid_alias('Q691','Independent State of Papua New Guinea').
wid_alias('Q424','Campuchia').
wid_alias('Q424','Kamboja').
wid_alias('Q424','Kingdom of Cambodia').
wid_alias('Q424','Kampuchea').
wid_alias('Q424','Cambodia').
wid_alias('Q424','Kambodzha').
wid_alias('Q424','Camboya').
wid_alias('Q233','Malta').
wid_alias('Q233','Republic of Malta').
wid_alias('Q221','FYROM').
wid_alias('Q221','Republic of North Macedonia').
wid_alias('Q221','North Macedonia').
wid_alias('Q221','FYR Macedonia').
wid_alias('Q221','Former Yugoslav Republic of Macedonia').
wid_alias('Q221','Former Yugoslav Republic Of Macedonia').
wid_alias('Q221','Republic of Macedonia').
wid_alias('Q423','North Korea').
wid_alias('Q423','Dem. Kore Cumhuriyeti').
wid_alias('Q423','Democratic People’s Republic of Korea').
wid_alias('Q423','KDHC').
wid_alias('Q423','DPRK').
wid_alias('Q423','Democratic People\'s Republic of Korea').
wid_alias('Q423','Democratic People\'s Republic Of Korea').
wid_alias('Q423','Kore Demokratik Halk Cumhuriyeti').
wid_alias('Q423','N. Korea').
wid_alias('Q423','Kuzey Kore').
wid_alias('Q733','Republic of Paraguay').
wid_alias('Q733','Heart of South America').
wid_alias('Q733','Paraguay').
wid_alias('Q1020','Malawi').
wid_alias('Q1020','Republic of Malawi').
wid_alias('Q986','State of Eritrea').
wid_alias('Q986','Iritriyā').
wid_alias('Q986','Eritrea').
wid_alias('Q986','Ertrā').
wid_alias('Q1025','Islamic Republic of Mauritania').
wid_alias('Q1025','Agawej').
wid_alias('Q1025','Muritanya').
wid_alias('Q1025','Moritani').
wid_alias('Q1025','Gànnaar').
wid_alias('Q1025','Murutaane').
wid_alias('Q1025','Gannaar').
wid_alias('Q1025','Mauritania').
wid_alias('Q754','Trinidad & Tobago').
wid_alias('Q754','Trinidad And Tobago').
wid_alias('Q754','Trinidad and Tobago').
wid_alias('Q754','Republic of Trinidad and Tobago').
wid_alias('Q685','Solomon Islands').
wid_alias('Q1013','Lesotho').
wid_alias('Q1013','Kingdom of Lesotho').
wid_alias('Q711','Mongolia').
wid_alias('Q711','Mongol uls').
wid_alias('Q1011','Republic of Cabo Verde').
wid_alias('Q1011','Republic of Cape Verde').
wid_alias('Q1011','Cape Verde').
wid_alias('Q1011','Cabo Verde').
wid_alias('Q242','Belize').
wid_alias('Q235','Principality of Monaco').
wid_alias('Q235','Principality and Diocese of Monaco').
wid_alias('Q235','Principato di Monaco').
wid_alias('Q235','Prinçipatu de Mùnegu').
wid_alias('Q235','Monaco').
wid_alias('Q235','Fort-Hercule').
wid_alias('Q235','Mùnegu').
wid_alias('Q235','Principatu de Mu̍negu').
wid_alias('Q235','Principauté de Monaco').
wid_alias('Q244','Barbados').
wid_alias('Q244','Barbadoes').
wid_alias('Q238','Most Serene Republic of San Marino').
wid_alias('Q238','San Marino').
wid_alias('Q217','Republic Of Moldova').
wid_alias('Q217','Republic of Moldova').
wid_alias('Q217','Moldavia').
wid_alias('Q217','Moldova').
wid_alias('Q217','Republica Moldova').
wid_alias('Q921','Brunei Darussalam').
wid_alias('Q921','Brunei').
wid_alias('Q921','Negara Brunei Darussalam').
wid_alias('Q921','Nation of Brunei, the Abode of Peace').
wid_alias('Q1050','Eswatini').
wid_alias('Q1050','Swaziland').
wid_alias('Q1050','Kingdom of Swaziland').
wid_alias('Q1050','eSwatini').
wid_alias('Q1050','Umbuso weSwatini').
wid_alias('Q1050','Kingdom of Eswatini').
wid_alias('Q734','Guyana').
wid_alias('Q734','Republic of Guyana').
wid_alias('Q734','Co-operative Republic of Guyana').
wid_alias('Q778','Bahamas').
wid_alias('Q778','Commonwealth of the Bahamas').
wid_alias('Q778','The Bahamas').
wid_alias('Q778','Bahama Islands').
wid_alias('Q697','Naoero').
wid_alias('Q697','Nauru').
wid_alias('Q697','Republic of Nauru').
wid_alias('Q697','Pleasant Island').
wid_alias('Q709','Republic of the Marshall Islands').
wid_alias('Q709','Marshall Islands').
wid_alias('Q709','Aolepān Aorōkin M̧ajeļ').
wid_alias('Q819','Lao People\'s Democratic Republic').
wid_alias('Q819','Lao PDR').
wid_alias('Q819','Laos').
wid_alias('Q819','United Republic Of Tanzania').
wid_alias('Q924','United Republic of Tanzania').
wid_alias('Q924','Tanzania').
wid_alias('Q924','United Republic of Tanganyika and Zanzibar').
wid_alias('Q683','Western Samoa').
wid_alias('Q683','Independent State of Samoa').
wid_alias('Q683','Samoa').
wid_alias('Q672','Tuvalu').
wid_alias('Q672','Ellice Islands').
wid_alias('Q695','Pelew').
wid_alias('Q695','Pelew islands').
wid_alias('Q695','Republic of Palau').
wid_alias('Q695','Palau').
wid_alias('Q695','Belau').
wid_alias('Q695','Republic of Belau').
wid_alias('Q678','Friendly Islands').
wid_alias('Q678','Tonga Islands').
wid_alias('Q678','Kingdom of Tonga').
wid_alias('Q678','Tonga').
wid_alias('Q228','Principality of Andorra').
wid_alias('Q228','Andorra').
wid_alias('Q228','Principat d\'Andorra').
wid_alias('Q228','Principality of the Valleys of Andorra').
wid_alias('Q874','Turkmenistan').
wid_alias('Q874','Turkmenia').
wid_alias('Q970','Union des Comores').
wid_alias('Q970','Union of the Comoros').
wid_alias('Q970','Juzur al-Qumur').
wid_alias('Q970','Comoros').
wid_alias('Q970','the Comoros').
wid_alias('Q970','Massiwa ya komori').
wid_alias('Q970','Juzur al-Qamar').
wid_alias('Q970','Udzima wa Komori').
wid_alias('Q970','ouvoimoja wakomori').
wid_alias('Q970','al-Ittiḥād al-Qumurī').
wid_alias('Q702','Fed. Sts. of Micronesia').
wid_alias('Q702','Federated States of Micronesia, FSM').
wid_alias('Q702','Micronesia').
wid_alias('Q702','Fed. Sts. Micronesia').
wid_alias('Q702','Federated States Of Micronesia').
wid_alias('Q702','Federated States of Micronesia').
wid_alias('Q781','Antigua And Barbuda').
wid_alias('Q781','Antigua and Barbuda').
wid_alias('Q1039','Democratic Republic of São Tomé and Príncipe').
wid_alias('Q1039','São Tomé og Príncipe').
wid_alias('Q1039','Sao Tome and Principe').
wid_alias('Q1039','Sao Tome & Principe').
wid_alias('Q1039','Sao Tome And Principe').
wid_alias('Q1039','São Tomé and Príncipe').
wid_alias('Q1042','Seychelles').
wid_alias('Q1042','Republic of Seychelles').
wid_alias('Q760','St Lucia').
wid_alias('Q760','Saint Lucia').
wid_alias('Q760','Hewanorra').
wid_alias('Q760','Iyonola').
wid_alias('Q760','St. Lucia (island)').
wid_alias('Q760','St. Lucia').
wid_alias('Q730','Surinam').
wid_alias('Q730','Republic of Suriname').
wid_alias('Q730','Dutch Guiana').
wid_alias('Q730','Republic of Surinam').
wid_alias('Q730','Suriname').
wid_alias('Q784','The Commonwealth of Dominica').
wid_alias('Q784','Wai\'tu kubuli').
wid_alias('Q784','Dominica').
wid_alias('Q784','Wai‘tu kubuli').
wid_alias('Q784','Commonwealth of Dominica').
wid_alias('Q917','Kingdom of Bhutan').
wid_alias('Q917','Bhutan').
wid_alias('Q769','Grenada').
wid_alias('Q763','St. Kitts & Nevis').
wid_alias('Q763','Saint Kitts and Nevis').
wid_alias('Q763','St. Kitts and Nevis').
wid_alias('Q763','Federation of Saint Kitts and Nevis').
wid_alias('Q763','Saint Christopher and Nevis').
wid_alias('Q763','St Kitts and Nevis').
wid_alias('Q763','Saint Kitts And Nevis').
wid_alias('Q763','Kitts & Nevis').
wid_alias('Q686','Republic of Vanuatu').
wid_alias('Q686','Vanuatu').
wid_alias('Q710','Kiribati').
wid_alias('Q710','Republic of Kiribati').
wid_alias('Q34020','Niue').
wid_alias('Q34020','Niuē').
wid_alias('Q26988','Cook Islands').
wid_alias('Q26988','Hervey Islands').
wid_alias('Q139377','Kingdom of Aksum').
wid_alias('Q139377','Aksum').
wid_alias('Q139377','Aksumite Empire').
wid_alias('Q139377','Kingdom of Axum').
wid_alias('Q139377','Axum').
wid_alias('Q146761','Republic of Hatay').
wid_alias('Q146761','Hatay State').
wid_alias('Q244165','Republic of Artsakh').
wid_alias('Q244165','Nagorno-Karabakh Republic').
wid_alias('Q244165','Artsakh Republic').
wid_alias('Q402145','Islamic Mataram Kingdom').
wid_alias('Q402145','Sultanate of Mataram').
wid_alias('Q402145','Mataram Sultanate').
wid_alias('Q402145','Mataram').
wid_alias('Q523380','Litwa Środkowa').
wid_alias('Q523380','Republic of Central Lithuania').
wid_alias('Q599613','County of Namur').
wid_alias('Q912052','Liang').
wid_alias('Q1063498','United States of the Ionian Islands').
wid_alias('Q1165546','Kingdom of Mosquitia').
wid_alias('Q2526751','Dál nAraidi').
wid_alias('Q2526751','Dal nAraidi').
wid_alias('Q2576045','Tartary Minor').
wid_alias('Q2576045','Crimean Khanate').
wid_alias('Q2576045','Lesser Tartary').
wid_alias('Q2576045','Tartaria Minor').
wid_alias('Q2576045','Minor Tartary').
wid_alias('Q2576045','Little Tartary').
wid_alias('Q6037274','Mosquito Territory').
wid_alias('Q6037274','Mosquito Coast').
wid_alias('Q6037274','Mosquito Shore').
wid_alias('Q6037274','Mosquitia').
wid_alias('Q5705','Catalunya').
wid_alias('Q5705','Autonomous Community of Catalonia').
wid_alias('Q5705','Catalonia').
wid_alias('Q5705','Catalonha').
wid_alias('Q5705','Cataluña').
wid_alias('Q21203','Aruba').
wid_alias('Q21203','America/Aruba').
wid_alias('Q21203','Island of Aruba').
wid_alias('Q22880','Kurpfalz').
wid_alias('Q22880','Palatinate of the Rhine').
wid_alias('Q22880','Rhenish County Palatine').
wid_alias('Q22880','Electorate of the Palatine').
wid_alias('Q22880','Electorate of the Palatinate').
wid_alias('Q22880','County Palatine of the Rhine').
wid_alias('Q22880','Palatine Electorate').
wid_alias('Q22880','Kurfürstentum von der Pfalz').
wid_alias('Q22880','County Palatinate of the Rhine').
wid_alias('Q22880','Electoral Palatinate').
wid_alias('Q22880','Electorate of Palatinate').
wid_alias('Q22880','Electorate the Palatinate').
wid_alias('Q22880','Electorate Palatinate').
wid_alias('Q22880','Palatinate of the rhein').
wid_alias('Q22880','County Palatine of Lotharingia').
wid_alias('Q22880','Pfalzgrafschaft bei Rhein').
wid_alias('Q22880','Bavarian Palatinate').
wid_alias('Q25279','Curaςao').
wid_alias('Q25279','Curocao').
wid_alias('Q25279','Curaçao').
wid_alias('Q25279','Curazao').
wid_alias('Q25279','Curacao').
wid_alias('Q25279','Curaçoa').
wid_alias('Q25279','Island Territory of Curaçao').
wid_alias('Q25279','Curacau').
wid_alias('Q25279','ISO 3166-1:CW').
wid_alias('Q25279','Kòrsou').
wid_alias('Q25279','Country of Curaçao').
wid_alias('Q26273','NL-SX').
wid_alias('Q26273','Sint Maarten').
wid_alias('Q26273','Saint Martin (Dutch part)').
wid_alias('Q26273','Sint Maarten (Dutch part)').
wid_alias('Q26273','Saint Martin').
wid_alias('Q26273','St Martin').
wid_alias('Q26273','St. Maarten').
wid_alias('Q25','Cambria').
wid_alias('Q25','Wales').
wid_alias('Q25','Cymru').
wid_alias('Q237','Vatican City State').
wid_alias('Q237','Stato della Città del Vaticano').
wid_alias('Q237','Vatican').
wid_alias('Q237','Papal State').
wid_alias('Q237','Vatican City').
wid_alias('Q237','Città del Vaticano').
wid_alias('Q237','Civitas Vaticana').
wid_alias('Q237','The Vatican').
wid_alias('Q237','Status Civitatis Vaticanae').
wid_alias('Q237','State of Vatican City').
wid_alias('Q180573','South Vietnam').
wid_alias('Q180573','Republic of Vietnam').
wid_alias('Q189988','Central African Empire').
wid_alias('Q190025','Federal Republic of Central America').
wid_alias('Q190025','United States of Central America').
wid_alias('Q449639','Alodia').
wid_alias('Q449639','Aloa').
wid_alias('Q449639','Alwa').
wid_alias('Q797422','Republic of Upper Volta').
wid_alias('Q797422','Upper Volta').
wid_alias('Q907112','Transdniestria').
wid_alias('Q907112','Moldavian Republic of Transdniestria').
wid_alias('Q907112','Republic of Pridnestrovie').
wid_alias('Q907112','Transdniester').
wid_alias('Q907112','Transnistria').
wid_alias('Q907112','Pridnestrovia').
wid_alias('Q907112','Trans-Dniestr').
wid_alias('Q907112','Pridnestrovie').
wid_alias('Q907112','Pridnestrovian Moldavian Republic').
wid_alias('Q907112','Transdniestrian Moldavian Republic').
wid_alias('Q907112','Pridnestria').
wid_alias('Q1147441','British Territories in Borneo').
wid_alias('Q1147441','British Borneo Territories').
wid_alias('Q1147441','British North Borneo').
wid_alias('Q1147441','State of British North Borneo').
wid_alias('Q1147441','Colony of North Borneo').
wid_alias('Q1155700','Rattanakosin Kingdom (1782–1932)').
wid_alias('Q1155700','Rattanakosin Kingdom').
wid_alias('Q1852345','Trucial Coast').
wid_alias('Q1852345','Trucial States').
wid_alias('Q1968554','Abazinia').
wid_alias('Q2988343','Melaka Portugis').
wid_alias('Q2988343','Malaca Portuguesa').
wid_alias('Q2988343','Portuguese Malacca').
wid_alias('Q3968612','Italian United Provinces').
wid_alias('Q12060881','Imperial China').
wid_alias('Q12060881','Chinese Empire').
wid_alias('Q114318415','Zaporozhye State').
wid_alias('Q114318415','Zaporizhzhia State').
wid_alias('Q115166787','Reman').
wid_alias('Q3908','Galicia (Spain)').
wid_alias('Q3908','Galiza').
wid_alias('Q3908','Galicia').
wid_alias('Q11774','Gupta Empire').
wid_alias('Q11774','Golden age of india').
wid_alias('Q11774','Gupta dynasty').
wid_alias('Q12548','First Reich').
wid_alias('Q12548','Old Empire').
wid_alias('Q12548','Roman-German Empire').
wid_alias('Q12548','Holy Roman Empire of the German Nation').
wid_alias('Q12548','Reich').
wid_alias('Q12548','Holy Roman Empire').
wid_alias('Q31747','Irish Free State').
wid_alias('Q40362','Sahrawi Arab Democratic Republic').
wid_alias('Q40362','Saharaui Arab Democratic Republic').
wid_alias('Q40362','SADR').
wid_alias('Q40362','Saharawi Arab Democratic Republic').
wid_alias('Q172640','Democratic Republic of Vietnam').
wid_alias('Q172640','North Vietnam').
wid_alias('Q172640','Vietnam (North)').
wid_alias('Q205718','Zirid Dynasty').
wid_alias('Q205718','Zirid polity').
wid_alias('Q205718','Zirid Kingdom').
wid_alias('Q213353','Cisalpine Republic').
wid_alias('Q747314','Bechuanaland Protectorate').
wid_alias('Q747314','Bechuana-land').
wid_alias('Q907234','French protectorate in Morocco').
wid_alias('Q907234','Protectorat français au Maroc et sa création par marshal leuté').
wid_alias('Q1146786','Señorío of Cuzcatlán').
wid_alias('Q1322662','Chernihiv principality').
wid_alias('Q1322662','Principality of Chernigov').
wid_alias('Q1415162','Garðaveldi').
wid_alias('Q1415162','Garðaríki').
wid_alias('Q1415162','Gardariki').
wid_alias('Q1415162','Gardarike').
wid_alias('Q1760729','Kingdom of Alashiya').
wid_alias('Q1760729','Alashiya').
wid_alias('Q2444884','Tibet').
wid_alias('Q2444884','Kingdom of Tibet').
wid_alias('Q2578028','Sikh Misl').
wid_alias('Q3167772','Kingdom of Polonnaruwa').
wid_alias('Q3780537','Gaza Empire').
wid_alias('Q8272919','Interwar Lithuania').
wid_alias('Q865','Formosa').
wid_alias('Q865','Chunghwa Minkuo').
wid_alias('Q865','Taivang').
wid_alias('Q865','Free area of the Republic of China').
wid_alias('Q865','Taivangʉ').
wid_alias('Q865','China, Republic').
wid_alias('Q865','Free China').
wid_alias('Q865','Republic of China').
wid_alias('Q865','Republic of China (Taiwan)').
wid_alias('Q865','Taywan').
wid_alias('Q865','Taiwan ROC').
wid_alias('Q865','Taiwan').
wid_alias('Q865','Chunghwa Minkwo').
wid_alias('Q865','Nanasian').
wid_alias('Q865','Chinese Taipei').
wid_alias('Q865','Ilaod').
wid_alias('Q865','Nationalist China').
wid_alias('Q971','Congo (Brazzaville)').
wid_alias('Q971','Congo Republic').
wid_alias('Q971','République du Congo').
wid_alias('Q971','Rep. Congo').
wid_alias('Q971','Republique du Congo').
wid_alias('Q971','Congo-Brazzaville').
wid_alias('Q971','Republic of Congo').
wid_alias('Q1019','Republic of Madagascar').
wid_alias('Q1019','Madagascar').
wid_alias('Q16644','the Northern Mariana Islands').
wid_alias('Q16644','Northern Mariana Islands').
wid_alias('Q16644','CNMI').
wid_alias('Q16644','Commonwealth of the Northern Maria Islands').
wid_alias('Q22','UK-SC').
wid_alias('Q22','UK-SCT').
wid_alias('Q22','Scotland').
wid_alias('Q22','Scotland, United Kingdom').
wid_alias('Q22','Scot').
wid_alias('Q22','Alba').
wid_alias('Q22','Caledonia').
wid_alias('Q21','England').
wid_alias('Q26','The North').
wid_alias('Q26','Tuaisceart Éireann').
wid_alias('Q26','North Ireland').
wid_alias('Q26','N. Ireland').
wid_alias('Q26','Northern Ireland').
wid_alias('Q26','Norlin Airlann').
wid_alias('Q80211','Lithuanian Soviet Socialist Republic').
wid_alias('Q80211','Lithuanian Soviet Socialist Republic (1918–1919)').
wid_alias('Q132856','Armenian Soviet Socialist Republic').
wid_alias('Q132856','Armenian SSR').
wid_alias('Q148540','Repubblica Fiorentina').
wid_alias('Q148540','Florentine Republic').
wid_alias('Q148540','Republic of Florence').
wid_alias('Q148540','Repubblica di Firenze').
wid_alias('Q164079','Hannover').
wid_alias('Q164079','Hanover').
wid_alias('Q164079','Königreich Hannover').
wid_alias('Q164079','Kingdom of Hanover').
wid_alias('Q245160','Democratic Republic of Georgia').
wid_alias('Q282475','Kingdom of Kartli-Kakheti').
wid_alias('Q282475','ქართლ-კახეთის სამეფო').
wid_alias('Q282475','Kingdom of Kartli and Kakheti').
wid_alias('Q282475','Kartli-Kakheti').
wid_alias('Q335088','Armenian Kingdom of Cilicia').
wid_alias('Q335088','Little Armenia').
wid_alias('Q529605','Hesse-Kassel').
wid_alias('Q529605','Hesse-Cassel').
wid_alias('Q529605','Electorate of Hesse').
wid_alias('Q1392840','Rurik\'s state').
wid_alias('Q1483495','Principality of Smolensk').
wid_alias('Q1483495','Grand Principality of Smolensk').
wid_alias('Q2268597','United Provinces of Central Italy').
wid_alias('Q2940142','Gothic Hispania').
wid_alias('Q2940142','Regnum Hispaniae').
wid_alias('Q2940142','Regnum Spaniae').
wid_alias('Q2940142','Regnum gothorum').
wid_alias('Q2940142','Kingdom of Toledo').
wid_alias('Q2940142','Kingdom of Hispania').
wid_alias('Q2940142','Visigothic Kingdom of Toledo').
wid_alias('Q3307686','Visigothic Kingdom of Tolosa').
wid_alias('Q4689103','Transitional Islamic State of Afghanistan').
wid_alias('Q4689103','Afghan Transitional Administration').
wid_alias('Q11362263','World Soviet Socialist Republic').
wid_alias('Q20949725','Emirate of Sperlinga').
wid_alias('Q20949725','Principauté de Sperlinga').
wid_alias('Q20949725','Principality of Sperlinga').
wid_alias('Q20949725','Isb.rl.nkah').
wid_alias('Q20949725','Principato di Sperlinga').
wid_alias('Q28233','Korean Empire').
wid_alias('Q28233','Taehan Cheguk').
wid_alias('Q28233','Daikan Teikoku').
wid_alias('Q37102','Republic of the Rif').
wid_alias('Q140359','Československá republika').
wid_alias('Q140359','First Czechoslovak Republic').
wid_alias('Q153080','East Francia').
wid_alias('Q153080','Kingdom of the East Franks').
wid_alias('Q156418','Kingdom of Hawaii').
wid_alias('Q156418','Hawaiian Kingdom').
wid_alias('Q156418','Kingdom of Hawaiʻi').
wid_alias('Q170072','United Provinces').
wid_alias('Q170072','Republic of the Seven United Provinces').
wid_alias('Q170072','Republic of the Seven United Netherlands').
wid_alias('Q170072','Dutch Republic').
wid_alias('Q170072','Republic of the Netherlands').
wid_alias('Q170072','Netherlands (1581–1795)').
wid_alias('Q170072','Federated Dutch Provinces').
wid_alias('Q170072','United Provinces of the Netherlands').
wid_alias('Q170072','Dutch Federation').
wid_alias('Q170072','Republic of the United Netherlands').
wid_alias('Q212056','Communist Mongolia').
wid_alias('Q212056','Bügd Nairamdakh Mongol Ard Uls').
wid_alias('Q212056','People\'s Republic of Mongolia').
wid_alias('Q212056','Socialist Mongolia').
wid_alias('Q212056','PR Mongolia').
wid_alias('Q212056','Mongolian People\'s Republic').
wid_alias('Q267584','Yemen Arab Republic').
wid_alias('Q267584','North Yemen').
wid_alias('Q303421','Kingdom of Galicia').
wid_alias('Q426025','Savoy').
wid_alias('Q426025','Duchy of Savoy').
wid_alias('Q431731','Republic of Tanganyika').
wid_alias('Q431731','Tanganyika').
wid_alias('Q444912','Vechiul Regat').
wid_alias('Q444912','Old Romanian Kingdom').
wid_alias('Q444912','Romanian Old Kingdom').
wid_alias('Q580188','Kingdom of Yemen').
wid_alias('Q580188','Mutawakkilite Kingdom of Yemen').
wid_alias('Q580188','Al-Mamlakah Al-Mutawakkilīyah Al-Yamanīyah').
wid_alias('Q671658','Republic of Yucatan').
wid_alias('Q671658','Republic of Yucatán').
wid_alias('Q684030','Serbian Principality').
wid_alias('Q684030','Kneževina Srbija').
wid_alias('Q684030','Principality of Serbia').
wid_alias('Q684030','Serb Principality').
wid_alias('Q738144','United Provinces of the Rio de la Plata').
wid_alias('Q738144','Provincias Unidas de Sudamérica').
wid_alias('Q738144','Provincias Unidas del Río de la Plata').
wid_alias('Q738144','Provincias Unidas del Rio de la Plata').
wid_alias('Q738144','Provincias Unidas de Sudamerica').
wid_alias('Q738144','United Provinces of the Río de la Plata').
wid_alias('Q738144','United Provinces of South America').
wid_alias('Q771193','Kingdom of Brittany').
wid_alias('Q964024','Moldavian Democratic Republic').
wid_alias('Q1057542','Republic of Hawaii').
wid_alias('Q1179410','De\'aruwa').
wid_alias('Q1179410','Piaroa').
wid_alias('Q1179410','De\'aruhua').
wid_alias('Q1179410','Huottuja').
wid_alias('Q1179410','Uwottuja').
wid_alias('Q1362278','United States of Stellaland').
wid_alias('Q1415128','Republic of Afghanistan').
wid_alias('Q2327097','Imperial City of Windsheim').
wid_alias('Q2578706','Kingdom of Ceredigion').
wid_alias('Q2578706','Ceredigion').
wid_alias('Q3136869','Kingdom of Dambadeniya').
wid_alias('Q4453003','Tashkent State').
wid_alias('Q5124786','Civitas Schinesghe').
wid_alias('Q12491063','Kerajaan Tulang Bawang').
wid_alias('Q17319155','Udaipur State').
wid_alias('Q17319155','Mewar State').
wid_alias('Q17319155','Mewar kingdom').
wid_alias('Q23366230','République de Genève').
wid_alias('Q23366230','Republic of Geneva').
wid_alias('Q96028967','Emirate of Nejd and Hasa (1913)').
wid_alias('Q96028967','Emirate of Nejd and Hasa').
wid_alias('Q5684','Babylon').
wid_alias('Q117020','Imperial City of Nuremberg').
wid_alias('Q117020','Free Imperial City of Nuremberg').
wid_alias('Q130229','Georgian SSR').
wid_alias('Q130229','Georgian Soviet Socialist Republic').
wid_alias('Q130229','Georgian S.S.R.').
wid_alias('Q130229','Soviet Georgia').
wid_alias('Q150981','North German Confederation').
wid_alias('Q150981','Norddeutscher Bund').
wid_alias('Q159631','Württemberg').
wid_alias('Q159631','Kingdom of Württemberg').
wid_alias('Q162192','Riograndense Republic').
wid_alias('Q162192','Piratini Republic').
wid_alias('Q165154','Piedmont-Sardinia').
wid_alias('Q165154','Kingdom of Sardinia').
wid_alias('Q165154','Kingdom of Piedmont-Sardinia').
wid_alias('Q165154','Kingdom of Sardinia (1720-1861)').
wid_alias('Q165154','Kingdom of Sardinia-Piedmont').
wid_alias('Q174306','Republic of Genoa').
wid_alias('Q187035','Principality of Albania').
wid_alias('Q203493','Kingdom of Romania').
wid_alias('Q203493','Romanian Kingdom').
wid_alias('Q203493','Kingdom of Rumania').
wid_alias('Q203493','Regatul României').
wid_alias('Q203493','Rumanian Kingdom').
wid_alias('Q252580','Duchy of Modena and Reggio').
wid_alias('Q268970','German-Austria').
wid_alias('Q268970','Republik Deutsch-Österreich').
wid_alias('Q268970','Republic of German-Austria').
wid_alias('Q550374','South African Republic').
wid_alias('Q550374','Zuid-Afrikaansche Republiek').
wid_alias('Q550374','The Transvaal').
wid_alias('Q550374','Republic of Transvaal').
wid_alias('Q550374','Transvaal Republic').
wid_alias('Q671362','Principality of Turov and Pinsk').
wid_alias('Q707128','Jeong-an kingdom').
wid_alias('Q715257','Later Silla').
wid_alias('Q715257','Unified Silla').
wid_alias('Q736727','Republic of Siena').
wid_alias('Q736727','Repubblica di Siena').
wid_alias('Q738264','State of Los Altos').
wid_alias('Q738264','Los Altos State').
wid_alias('Q842091','Duchy of Normandy').
wid_alias('Q842091','Normandy').
wid_alias('Q878319','Serbian Despotate').
wid_alias('Q976099','Malayan Union').
wid_alias('Q1054184','Khmer Republic').
wid_alias('Q1152126','Republique populaire du Congo').
wid_alias('Q1152126','République populaire du Congo').
wid_alias('Q1152126','People\'s Republic of the Congo').
wid_alias('Q2273304','Moravian Serbia').
wid_alias('Q3267672','Republic of Entre Ríos').
wid_alias('Q3267672','Republic of Entre Rios').
wid_alias('Q6773257','Eastern Karakhanids Khanate').
wid_alias('Q9063401','Federated States of Central America').
wid_alias('Q9063401','United Provinces of Central America').
wid_alias('Q11681694','Republic of Spanish Haiti').
wid_alias('Q11681694','República del Haití Español').
wid_alias('Q13474305','Francoist Spain').
wid_alias('Q13474305','Franco\'s Spain').
wid_alias('Q13474305','Franco\'s Dictatorship').
wid_alias('Q13474305','Nationalist Spain').
wid_alias('Q13474305','Spain under Franco').
wid_alias('Q15102440','Kingdom of Serbs, Croatians and Slovenes').
wid_alias('Q15102440','Kingdom of Serbs, Croats and Slovenes').
wid_alias('Q15102440','Kingdom SCS').
wid_alias('Q18285930','German Empire of 1848/1849').
wid_alias('Q7313','Yuan').
wid_alias('Q7313','Yuan dynasty').
wid_alias('Q7313','Great Yuan Great Mongol State').
wid_alias('Q7313','Great Yuan Empire').
wid_alias('Q7313','Yüan').
wid_alias('Q7313','Yuan Empire').
wid_alias('Q7313','Yuan imperial dynasty').
wid_alias('Q7313','Great Yuan').
wid_alias('Q7313','Empire of the Great Khan').
wid_alias('Q9903','Ming imperium').
wid_alias('Q9903','Empire of the Great Ming').
wid_alias('Q9903','Ming period').
wid_alias('Q9903','Ming-era').
wid_alias('Q9903','Ming dynasty').
wid_alias('Q9903','Ming China').
wid_alias('Q9903','Ming era').
wid_alias('Q9903','Great Ming').
wid_alias('Q9903','Ming Empire').
wid_alias('Q33946','People\'s Republic of Czechoslovakia').
wid_alias('Q33946','ČSFR').
wid_alias('Q33946','Federation of Czechoslovakia').
wid_alias('Q33946','Czechoslovakia').
wid_alias('Q33946','Czecho-Slovakia').
wid_alias('Q33946','Československo').
wid_alias('Q33946','CSFR').
wid_alias('Q39473','Siebenbürgen').
wid_alias('Q39473','Transylvania').
wid_alias('Q62389','Þjóðveldið Ísland').
wid_alias('Q62389','Icelandic Free State').
wid_alias('Q62389','Icelandic Commonwealth').
wid_alias('Q62389','Free State of Iceland').
wid_alias('Q68678','SSRB').
wid_alias('Q68678','Socialist Soviet Republic of Byelorussia').
wid_alias('Q130280','Estonian ssr').
wid_alias('Q130280','Estonian S.S.R').
wid_alias('Q130280','Estonian Soviet Socialist Republic').
wid_alias('Q130280','Estonian SSR').
wid_alias('Q130280','Soviet Estonia').
wid_alias('Q147909','Kingdom of Bulgaria').
wid_alias('Q147909','Tsarstvo Balgariya').
wid_alias('Q147909','Bulgarian Kingdom').
wid_alias('Q173065','Kingdom of Sicily').
wid_alias('Q173065','Kingdom of Naples').
wid_alias('Q173065','Regno di Napoli').
wid_alias('Q173065','Sicily, Kingdom of').
wid_alias('Q180393','Due Sicilie').
wid_alias('Q180393','Regno delle Due Sicilie').
wid_alias('Q180393','Kingdom of the Two Sicilies').
wid_alias('Q180393','Two Sicilies').
wid_alias('Q180393','Kingdom of Two Sicilies').
wid_alias('Q186096','Tsardom of Rus').
wid_alias('Q186096','Tsardom of Muscovy').
wid_alias('Q186096','Tsardom of Russia').
wid_alias('Q193152','Great Moravia').
wid_alias('Q193152','Great Moravian Empire').
wid_alias('Q216632','Mali Federation').
wid_alias('Q216632','Federation du Mali').
wid_alias('Q216632','Fédération du Mali').
wid_alias('Q218023','Oranje-Vrystaat').
wid_alias('Q218023','Oranje-Vrijstaat').
wid_alias('Q218023','Orange Free State').
wid_alias('Q243652','Republic of Councils in Hungary').
wid_alias('Q243652','Soviet Republic of Hungary').
wid_alias('Q243652','Hungarian Soviet Republic').
wid_alias('Q243652','Magyarországi Tanácsköztársaság').
wid_alias('Q370372','Emirate of Cyrenaica').
wid_alias('Q427941','Islamic Emirate of Afghanistan').
wid_alias('Q497777','First Spanish Republic').
wid_alias('Q862701','People\'s Republic of Benin').
wid_alias('Q870055','Kingdom of Laos').
wid_alias('Q903779','Reino Unido de Portugal, Brasil e Algarves').
wid_alias('Q903779','United Kingdom of Portugal, Brazil and the Algarves').
wid_alias('Q953432','Couto Misto').
wid_alias('Q1106424','Dervish sultanate').
wid_alias('Q1106424','Darawiish kingdom of Diiriye Guure').
wid_alias('Q1106424','Darawiish sultanate of Diiriye Guure').
wid_alias('Q1106424','Darawiish kingdom').
wid_alias('Q1106424','Dawlāt ad-Darāwīsh').
wid_alias('Q1106424','Darawiish').
wid_alias('Q1106424','Darwiish State').
wid_alias('Q1106424','Dervish Kingdom').
wid_alias('Q1106424','Dawlada Daraawiish').
wid_alias('Q1106424','Dervish kingdom of Diiriye Guure').
wid_alias('Q1106424','Daraawiish kingdom of Diiriye Guure').
wid_alias('Q1530762','Kingdom of Kandy').
wid_alias('Q2393278','Republic of Cospaia').
wid_alias('Q2899771','Gyula/Kende of the Hungarians').
wid_alias('Q2899771','Duchy of Hungary').
wid_alias('Q2899771','Principality of Hungary').
wid_alias('Q2899771','Grand Principality of Hungary').
wid_alias('Q3571951','Lordship of Molina').
wid_alias('Q3571951','Royal Lordship of Molina').
wid_alias('Q3572054','Lordship of Albarracin').
wid_alias('Q3845765','Marquisate of Zuccarello').
wid_alias('Q3892131','Provincia del Guayas').
wid_alias('Q3892131','País Libre de Guayaquil').
wid_alias('Q3892131','Provincia Libre de Guayaquil').
wid_alias('Q3892131','República del Guayaquil').
wid_alias('Q4147013','Tulunid emirate').
wid_alias('Q4304392','Moscovian State').
wid_alias('Q4304392','Russian State').
wid_alias('Q4304392','Moscow Ulus').
wid_alias('Q4445623','Principality of Suzdal').
wid_alias('Q4445623','Duchy of Suzdal').
wid_alias('Q5362837','Emirate of Jabal Shammar').
wid_alias('Q6207845','Mandore kingdom').
wid_alias('Q6207845','Kingdom of Marwar').
wid_alias('Q6207845','Marwar Kingdom').
wid_alias('Q6207845','Jodhpur State').
wid_alias('Q6207845','Jodhpur').
wid_alias('Q8842943','Estado Libre del Istmo').
wid_alias('Q8842943','The Free State of the Isthmus').
wid_alias('Q10975458','Maramorshchyna').
wid_alias('Q10975458','Máramaros').
wid_alias('Q10975458','Maramureș').
wid_alias('Q10975458','Marmaroshchyna').
wid_alias('Q11938308','Nabhani dynasty').
wid_alias('Q12491044','Negara Dipa').
wid_alias('Q16550783','Anhalt').
wid_alias('Q16550783','Duchy of Anhalt').
wid_alias('Q30747910','Islamic Republic of Afghanistan').
wid_alias('Q4948','Venice').
wid_alias('Q4948','Republic of Saint Mark').
wid_alias('Q4948','Venezia Republic').
wid_alias('Q4948','Republic of Venezia').
wid_alias('Q4948','Republic Venice').
wid_alias('Q4948','Serenissima Repubblica di Venezia').
wid_alias('Q4948','Republic of St. Mark').
wid_alias('Q4948','Serenìsima Repùblega de Venèsia').
wid_alias('Q4948','The Most Serene Republic of Venice').
wid_alias('Q4948','Duchy of Venetia').
wid_alias('Q4948','Republic of Venice').
wid_alias('Q4948','Venetian Empire').
wid_alias('Q4948','The Republic of Venice').
wid_alias('Q4948','The Serenissima').
wid_alias('Q4948','Venetian Republic').
wid_alias('Q4948','Venice Republic').
wid_alias('Q4948','Most Serene Republic of Venice').
wid_alias('Q4948','Repulic of Venice').
wid_alias('Q15180','The Soviets').
wid_alias('Q15180','Soviet Union').
wid_alias('Q15180','U.S.S.R.').
wid_alias('Q15180','the Union of Soviet Socialist Republics').
wid_alias('Q15180','CCCP').
wid_alias('Q15180','U.S.S.R').
wid_alias('Q15180','USSR').
wid_alias('Q15180','the Soviet Union').
wid_alias('Q15180','Union of Soviet Socialist Republics').
wid_alias('Q15180','Soviets').
wid_alias('Q38872','Preussen').
wid_alias('Q38872','Prussia').
wid_alias('Q38872','Prussia (Germany)').
wid_alias('Q121932','People\'s Republic of Bulgaria').
wid_alias('Q121932','Socialist Bulgaria').
wid_alias('Q121932','Communist Bulgaria').
wid_alias('Q121932','PR Bulgaria').
wid_alias('Q121932','Narodna republika Balgariya').
wid_alias('Q139319','Republic of Russia').
wid_alias('Q139319','Rossiyskaya respublika').
wid_alias('Q139319','Russian Republic').
wid_alias('Q151536','Novgorod Republic').
wid_alias('Q170468','United Arab Republic').
wid_alias('Q170588','Republic of Texas').
wid_alias('Q170588','Not Texas').
wid_alias('Q170770','Grand Principality of Moscow').
wid_alias('Q170770','Grand Duchy of Moscow').
wid_alias('Q170770','Muscovy').
wid_alias('Q172107','Polish–Lithuanian Commonwealth').
wid_alias('Q172107','Polish-Lithuanian Commonwealth').
wid_alias('Q172107','Republic of Both Nations').
wid_alias('Q172107','Commonwealth of Poland').
wid_alias('Q172107','Poland-Lithuania').
wid_alias('Q176495','Ständestaat').
wid_alias('Q176495','Federal State of Austria').
wid_alias('Q185488','Volga Bulgaria').
wid_alias('Q199821','Great Colombia').
wid_alias('Q199821','Gran Colombia').
wid_alias('Q207272','II Rzeczpospolita').
wid_alias('Q207272','Second Polish Republic').
wid_alias('Q217230','Império do Brasil').
wid_alias('Q217230','Imperio do Brazil').
wid_alias('Q217230','Imperio do Brasil').
wid_alias('Q217230','Brazilian Empire').
wid_alias('Q217230','Empire of Brazil').
wid_alias('Q221457','Congress Poland').
wid_alias('Q221457','Congress Kingdom of Poland').
wid_alias('Q221457','Russian Poland').
wid_alias('Q221457','Kingdom of Poland').
wid_alias('Q221457','Tsardom of Poland').
wid_alias('Q330988','Democratic Kampuchea').
wid_alias('Q457167','West Ukrainian People\'s Republic').
wid_alias('Q457167','West Ukraine').
wid_alias('Q457167','Zakhidno-Ukrainska Narodnia Respublika').
wid_alias('Q457167','Zakhidnoukrayins’ka Narodna Respublyka').
wid_alias('Q457167','ZUNR').
wid_alias('Q457167','Western Ukrainian National Republic').
wid_alias('Q494625','Usan-guk').
wid_alias('Q494625','Usan').
wid_alias('Q545205','Transcaucasian Socialist Federative Soviet Republic').
wid_alias('Q704300','Free City of Frankfurt').
wid_alias('Q814959','Peiyang Government').
wid_alias('Q814959','First Republic of China (1912-1928)').
wid_alias('Q814959','Beiyang Government').
wid_alias('Q1361989','First Philippine Republic').
wid_alias('Q1361989','Philippine Republic').
wid_alias('Q1361989','Malolos Republic').
wid_alias('Q1483430','Principality of Kyiv').
wid_alias('Q1483430','Kyiv principality').
wid_alias('Q1483430','Duchy of Kyiv').
wid_alias('Q1508143','Ukrainian State').
wid_alias('Q1508143','Hetman state').
wid_alias('Q1508143','Hetman government').
wid_alias('Q1508143','Hetmanshchyna').
wid_alias('Q1508143','Hetmanate').
wid_alias('Q2300246','Kingdom of Fez').
wid_alias('Q2454585','Third Czechoslovak Republic').
wid_alias('Q3324546','Sovereign Council of Asturias and León').
wid_alias('Q3324546','Sovereign Council of Asturias and Leon').
wid_alias('Q3623202','District of Branković').
wid_alias('Q4453007','Tashkent Khanate').
wid_alias('Q8575586','Umayyad Caliphate').
wid_alias('Q8575586','Caliphate of Damascus').
wid_alias('Q8575586','Umayyad Empire').
wid_alias('Q18590086','County of Loano').
wid_alias('Q39501433','State of Venezuela').
wid_alias('Q110362913','Chacha Empire').
wid_alias('Q5743','Ebla').
wid_alias('Q5743','Tell Mardikh').
wid_alias('Q7318','Hitler\'s Third Reich').
wid_alias('Q7318','Third Reich').
wid_alias('Q7318','Nazi Germany').
wid_alias('Q7318','Greater German Reich').
wid_alias('Q8733','Dinastía Qing o Ts\'ing').
wid_alias('Q8733','Great Qing state').
wid_alias('Q8733','Great Qing').
wid_alias('Q8733','Ch\'ing China').
wid_alias('Q8733','Manchu China').
wid_alias('Q8733','Manchu Dynasty').
wid_alias('Q8733','Manchu Tartar dynasty').
wid_alias('Q8733','Tartar-Manchu dynasty').
wid_alias('Q8733','Manchoo Tartar dynasty').
wid_alias('Q8733','Ta-tsing dynasty').
wid_alias('Q8733','Tai-tsing dynasty').
wid_alias('Q8733','Manchu Empire').
wid_alias('Q8733','Mantchoo Tartar dynasty').
wid_alias('Q8733','Empire of the Great Qing').
wid_alias('Q8733','Ta Tsing Empire').
wid_alias('Q8733','Tartar Chinese Empire').
wid_alias('Q8733','Tai Ching Ti Kuo').
wid_alias('Q8733','Qing Empire').
wid_alias('Q8733','Ch\'ing Empire').
wid_alias('Q8733','Mantchoo dynasty').
wid_alias('Q8733','Manchoo dynasty').
wid_alias('Q8733','Qing China').
wid_alias('Q8733','Ch\'ing Dynasty').
wid_alias('Q8733','Chinese-Tartar empire').
wid_alias('Q8733','Tartar-Mantchoo dynasty').
wid_alias('Q8733','Qing dynasty').
wid_alias('Q15864','United Kingdom of the Netherlands').
wid_alias('Q71234','Corsican Republic').
wid_alias('Q81931','the Southern Confederacy').
wid_alias('Q81931','the seceding States').
wid_alias('Q81931','the Confederate States of America').
wid_alias('Q81931','the Confederate States').
wid_alias('Q81931','the rebellious States').
wid_alias('Q81931','the Confederacy').
wid_alias('Q81931','Confederacy').
wid_alias('Q81931','Confederate States').
wid_alias('Q81931','C.S.A.').
wid_alias('Q81931','the States in rebellion').
wid_alias('Q81931','Confederate States of America').
wid_alias('Q81931','C.S.').
wid_alias('Q156513','Banat Republic').
wid_alias('Q178469','Biafra').
wid_alias('Q210036','ChRI').
wid_alias('Q210036','Ichkeria').
wid_alias('Q210036','Chechen Republic of Ichkeria').
wid_alias('Q211853','Republic of Serbian Krajina').
wid_alias('Q242036','Tamil Eelam').
wid_alias('Q266657','Labin Republic').
wid_alias('Q319881','Formosan Republic').
wid_alias('Q319881','Democratic State of Taiwan').
wid_alias('Q319881','Republic of Formosa').
wid_alias('Q319881','Republic of Taiwan').
wid_alias('Q319881','Taiwan Republic').
wid_alias('Q330756','Transcaucasian Democratic Federative Republic').
wid_alias('Q376778','United Baltic Duchy').
wid_alias('Q376778','Grand Duchy of Livonia').
wid_alias('Q516160','First Hungarian Republic').
wid_alias('Q516160','Hungarian People\'s Republic').
wid_alias('Q615902','Kingdom of Finland').
wid_alias('Q718374','State of Slovenes, Croats and Serbs').
wid_alias('Q784851','Turkish Federated State of Cyprus').
wid_alias('Q842199','Belarusian Democratic Republic').
wid_alias('Q842199','Belarusian People\'s Republic').
wid_alias('Q855044','Bhopal').
wid_alias('Q855044','Bhopal State').
wid_alias('Q867778','People\'s Republic of Kampuchea').
wid_alias('Q907827','Bremen Council Republic').
wid_alias('Q907827','Bremen Soviet Republic').
wid_alias('Q986665','East Turkistan').
wid_alias('Q986665','First East Turkestan Republic').
wid_alias('Q986665','Islamic Republic of East Turkistan').
wid_alias('Q1072140','Seconda Repubblica Romana').
wid_alias('Q1072140','Repubblica Romana del 1849').
wid_alias('Q1072140','Roman Republic').
wid_alias('Q1072140','Roman Republic (1849)').
wid_alias('Q1072140','Repubblica romana (1849)').
wid_alias('Q1072140','Repubblica Romana').
wid_alias('Q1148907','Katanga').
wid_alias('Q1148907','Republic of Katanga').
wid_alias('Q1148907','State of Katanga').
wid_alias('Q1240096','Hyderabad State').
wid_alias('Q1240096','Hyderabad').
wid_alias('Q1240096','Hyderabad Kingdom').
wid_alias('Q1240096','The Nizam\'s Dominions').
wid_alias('Q1540575','The Gozitan Nation').
wid_alias('Q1540575','Gozo').
wid_alias('Q1540575','Gozitania').
wid_alias('Q1540575','In-Nazzjon Għawdxija').
wid_alias('Q1540575','La Nazione Gozitana').
wid_alias('Q1549254','Principality of Seborga').
wid_alias('Q1549254','Seborga').
wid_alias('Q1649602','Republic of Vermont').
wid_alias('Q1649602','Republic of New Connecticut').
wid_alias('Q1649602','Vermont Republic').
wid_alias('Q1649602','State of Vermont').
wid_alias('Q1649602','New Connecticut Republic').
wid_alias('Q1649602','New Connecticut').
wid_alias('Q1811029','Finnish Socialist Workers\' Republic').
wid_alias('Q2119459','Democratic Republic of Yemen').
wid_alias('Q2182946','Commune of the Working People of Estonia').
wid_alias('Q2320255','Islamic Emirate of Waziristan').
wid_alias('Q2387250','State of Cambodia').
wid_alias('Q2453974','Don Republic').
wid_alias('Q2453974','Vsevelikoe Voisko Donskoe').
wid_alias('Q2453974','Almighty Don Host').
wid_alias('Q2842000','Amba Land').
wid_alias('Q2842000','Ambazonia').
wid_alias('Q2842000','Federal Republic of Ambazonia').
wid_alias('Q3112525','government of Kentucky').
wid_alias('Q3112525','Provisional Government of Kentucky').
wid_alias('Q3112525','state government of Kentucky').
wid_alias('Q3304333','Republic of Indian Stream').
wid_alias('Q3456497','Italian Partisan Republics').
wid_alias('Q4120908','Inner Mongolian People\'s Republic').
wid_alias('Q6106050','Republic of Floridas').
wid_alias('Q6106050','Republic of the Floridas').
wid_alias('Q6106050','Supreme Council of the Floridas').
wid_alias('Q6123746','zone under Republican control').
wid_alias('Q6123746','Red zone').
wid_alias('Q6123746','Republican zone').
wid_alias('Q6585262','Provisional Government of National Union and National Salvation of Cambodia').
wid_alias('Q6585262','PGNUNSC').
wid_alias('Q7314467','Republic of Florida').
wid_alias('Q7314468','Republic of Galicia').
wid_alias('Q7489715','Sharifian Caliphate').
wid_alias('Q7489715','Hashimite Caliphate').
wid_alias('Q11950523','Sumapaz').
wid_alias('Q13258120','Republic of West Florida').
wid_alias('Q13258120','West Florida').
wid_alias('Q15925436','Republic of Crimea').
wid_alias('Q16150196','the Donetsk People\'s Republic').
wid_alias('Q16150196','Donetsk People\'s Republic').
wid_alias('Q16150196','Donetsk Republic').
wid_alias('Q16250774','Kharkov People\'s Republic').
wid_alias('Q16250774','Kharkiv People\'s Republic').
wid_alias('Q16250774','People\'s Republic of Kharkov').
wid_alias('Q16250774','People\'s Republic of Kharkiv').
wid_alias('Q16673775','Kingdom of Kampuchea').
wid_alias('Q16746854','Luhansk People\'s Republic').
wid_alias('Q16746854','Luhansk Republic').
wid_alias('Q16746854','Lugansk People\'s Republic').
wid_alias('Q18331029','Hungarian State').
wid_alias('Q21076535','Islamic Republic of Qaim').
wid_alias('Q23906811','Republic of Dar El Kuti').
wid_alias('Q23906811','Dar El Kuti').
wid_alias('Q23906811','Republic of Logone').
wid_alias('Q24948646','Polish Socialistic Soviet Republic').
wid_alias('Q31354462','Abkhaziya').
wid_alias('Q31354462','Republic of Abkhazia').
wid_alias('Q31354462','Abkhazian Republic').
wid_alias('Q31354462','Aṗsny').
wid_alias('Q31354462','Республика Абхазия').
wid_alias('Q31354462','Apsny').
wid_alias('Q31354462','Aṗsny Ahwyntqarra').
wid_alias('Q31354462','Abkhazia').
wid_alias('Q31354462','Абхазия').
wid_alias('Q31354462','Аҧсны').
wid_alias('Q31354462','The Republic of Abkhazia').
wid_alias('Q31354462','Respublika Abkhaziya').
wid_alias('Q31354462','Аԥсны Аҳәынҭқарра').
wid_alias('Q65079691','Western Togoland').
wid_alias('Q86750256','Free Dadra and Nagar Haveli').
wid_alias('Q111967268','Islamic government of Laristan').
wid_alias('Q111967268','Laristan nation').
wid_alias('Q111967268','Islamic nation').
wid_alias('Q111967268','Islamic state of Laristan').
wid_alias('Q114318324','Kherson State').
wid_alias('Q5481','Tatarstan').
wid_alias('Q5481','Republic of Tatar').
wid_alias('Q5481','Tataria').
wid_alias('Q5481','Republic of Tatarstan').
wid_alias('Q5481','Tatariya').
wid_alias('Q11196','Republic of Srpska').
wid_alias('Q11196','Srpska').
wid_alias('Q11196','Serb Republic').
wid_alias('Q11196','Republika Srpska').
wid_alias('Q23427','the State of Alania').
wid_alias('Q23427','South Ossettia').
wid_alias('Q23427','State of Alania').
wid_alias('Q23427','South Ossetia').
wid_alias('Q23427','S Ossetia').
wid_alias('Q23427','South Osetia').
wid_alias('Q23427','S. Ossetia').
wid_alias('Q23427','Republic of South Ossetia').
wid_alias('Q23427','South Ossetian Republic').
wid_alias('Q23427','Alania').
wid_alias('Q23427','Republic of South Ossetia – the State of Alania').
wid_alias('Q23427','Tskhinvali Region').
wid_alias('Q23427','South Ossetian').
wid_alias('Q23427','Samachablo').
wid_alias('Q23681','Turkish Cyprus').
wid_alias('Q23681','Turkish Cypriot state').
wid_alias('Q23681','TRNC').
wid_alias('Q23681','Turkish Republic of Cyprus').
wid_alias('Q23681','North Cyprus').
wid_alias('Q23681','The Turkish Republic of Northern Cyprus').
wid_alias('Q23681','Turkish Federative State of North Cyprus').
wid_alias('Q23681','North Cyprus Turkish Republic').
wid_alias('Q23681','Turkish occupied Cyprus').
wid_alias('Q23681','Turkish Republic of North Cyprus').
wid_alias('Q23681','Republic of Northern Cyprus').
wid_alias('Q23681','Turkish Cypriot constituent state').
wid_alias('Q23681','Northern Cyprus').
wid_alias('Q34754','Republic of Somaliland').
wid_alias('Q34754','Somaliland').
