#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Author: Stian Rødven-Eide

# We use regular expressions to escape single quotes
# from the input string
import re
from pyswip import Prolog


def query_example(countryname):
    # Load an SWI-Prolog instance
    prolog = Prolog()
    # Loading the Prolog data
    prolog.consult('wdlabel.pl')
    prolog.consult('wid_alias.pl')
    # We need to escape potential single quotes in the country name
    countryname = re.sub(r'\'',r"\\'",countryname)
    # Find out the Wikidata-ID for the input country
    query = "wid_wdlabel(Wid,'{}').".format(countryname)
    result = prolog.query(query)
    wid = list(result)[0]['Wid']
    print("The Wikidata-ID for {} is {}".format(countryname,wid))
    # Retrieve aliases for Ukraine, using its Wikidata-ID
    query = "wid_alias('{}',Alias).".format(wid)
    result = prolog.query(query)
    aliases = [a['Alias'] for a in list(result)]
    print("The following aliases exist for {} ({}):".format(countryname,wid))
    for alias in aliases:
        print(alias)
    print("------------------------------------------------------")

        
def rule_example(countryname):
    # Load an SWI-Prolog instance
    prolog = Prolog()
    # Loading the Prolog data
    prolog.consult('wdlabel.pl')
    prolog.consult('wid_alias.pl')
    # We need to escape potential single quotes in the country name
    countryname = re.sub(r'\'',r"\\'",countryname)
    # Assert a rule that lets you get aliases from string,
    # without having to first retrieve the Wikidata-ID
    prolog.assertz("cname_alias(Country,Alias) :- \
                      wid_wdlabel(Wid,Country), \
                      wid_alias(Wid,Alias)")
    # Use the new rule to get aliases for the input country
    query = "cname_alias('{}',Alias).".format(countryname)
    result = prolog.query(query)
    aliases = [a['Alias'] for a in list(result)]
    print("The following aliases exist for {}:".format(countryname))
    for alias in aliases:
        print(alias)
    print("------------------------------------------------------")

    
def membership_of_country(countryname):
    # Load an SWI-Prolog instance    
    prolog = Prolog()
    # Loading the Prolog data
    prolog.consult('wdlabel.pl')
    prolog.consult('membership.pl')
    # We need to escape potential single quotes in the
    # country name
    countryname = re.sub(r'\'',r"\\'",countryname)
    # Find out the Wikidata-ID for the input country
    query = "wid_wdlabel(Wid,'{}').".format(countryname)
    result = prolog.query(query)
    wid = list(result)[0]['Wid']
    # Find out if and when the input country was a member of the Security Council
    query = "membership(Permanency,Group,ArabNations,'{}',FromDate,ToDate).".format(wid)
    result = prolog.query(query)
    data = list(result)
    if not data:
        print("{} was never a member of the Security Council between 1995 and 2023".format(countryname))
    else:
        for d in data:
            permanency = d['Permanency']
            group = d['Group']
            arab = d['ArabNations']
            fyear = str(d['FromDate'])[:4]
            fmonth = str(d['FromDate'])[4:6]
            fday = str(d['FromDate'])[6:]
            fdate = "{}-{}-{}".format(fyear,fmonth,fday)
            tyear = str(d['ToDate'])[:4]
            tmonth = str(d['ToDate'])[4:6]
            tday = str(d['ToDate'])[6:]
            tdate = "{}-{}-{}".format(tyear,tmonth,tday)
            print("{} was a {} member from the {} group between {} and {}".format(countryname,permanency,group.title(),fdate,tdate))
    print("------------------------------------------------------")


def membership_at_time(date):
    # Load an SWI-Prolog instance    
    prolog = Prolog()
    # Loading the Prolog data
    prolog.consult('wdlabel.pl')
    prolog.consult('membership.pl')
    # Construct a rule
    prolog.assertz("members_at_date(Member,Date,Group) :- \
                      membership(_,Group,_,Wid,From,To), \
                      Date >= From, \
                      Date =< To, \
                      wid_wdlabel(Wid,Member)")
    query = "members_at_date(Member,{},Group).".format(date)
    result = prolog.query(query)
    members = [(c['Member'],c['Group']) for c in list(result)]
    dstring = str(date)
    readable_date = "{}-{}-{}".format(dstring[:4],dstring[4:6],dstring[6:])
    print("The following countries were a member of the Security Council on {}:".format(readable_date))
    for (member,group) in members:
        if group == 'none':
            print("{} (Permanent member)".format(member))
        else:
            print("{} ({} group)".format(member,group.title()))
                   
            
if __name__ == '__main__':
    query_example('Ukraine')
    rule_example('Ukraine')
    membership_of_country('Ukraine')
    membership_at_time(20140301)
