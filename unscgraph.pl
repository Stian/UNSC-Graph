:- module(unscgraph, 
	[  country_as_tag/2,
	   country_as_topic/2,
	   country_mention/6,
	   membership/6,
	   presidency/2,
	   speech_sentiment/3,
	   spv_date/2,
	   spv_speech_wid/3,
	   spv_tag/2,
	   spv_tlt/2,
	   wid_unterm/2,
	   wid_wdlabel/2,
	   wid_alias/2,
	   wid_neighbour/2,
	   is_alias/2,
	   was_member/3,
	   members_at_date/2
	]).

:- [country_as_tag].
:- [country_as_topic].
:- [country_mention].
:- [membership].
:- [presidency].
:- [speech_sentiment].
:- [spv_date].
:- [spv_speech_wid].
:- [spv_tag].
:- [top_level_topic].
:- [unterm].
:- [wdlabel].
:- [wid_alias].
:- [wid_neighbour].


is_alias(CountryA,CountryB) :-
    wid_alias(Wid,CountryA),
    wid_alias(Wid,CountryB).

was_member(CountryName,From,To) :-
    wid_alias(Wid,CountryName),
    membership(_,_,_,Wid,From,To).

members_at_date(Member,Date) :-
    membership(_,_,_,Wid,From,To),
    Date >= From,
    Date =< To,
    wid_wdlabel(Wid,Member).
